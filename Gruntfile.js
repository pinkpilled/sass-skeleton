'use strict';


module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);


  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    /**
     * Set project info
     */
    project: {
      src: 'src',
      app: 'app',
      css: [
        'sass/style.scss'
      ],
    },

    /**
     * SCSSLint
     * Comprobador de estilo de código para SASS
     * Usa la configuración en .scss-lint.yml
     * 
     */
    scsslint: {
      allFiles: [
        'sass/main.scss',
      ],
      options: {
        bundleExec: true,
        config: '.scss-lint.yml',
        reporterOutput: 'scss-lint-report.xml',
        colorizeOutput: true
      },
    },

    /**
     * Compilar SASS
     * https://github.com/gruntjs/grunt-contrib-sass
     */
    sass: {
      dev: {
        options: {
          style: 'expanded'
        },
        files: {
          'css/style.unprefixed.css': 'sass/main.scss'
        }
      },
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'css/style.unprefixed.css': 'sass/main.scss'
        }
      }
    },

    /**
     * Autoprefixer
     * Añade vendor prefixes automáticamente
     * https://github.com/nDmitry/grunt-autoprefixer
     */
    autoprefixer: {
      options: {
        browsers: [
          'last 2 version',
          'safari 6',
          'ie 9',
          'opera 12.1',
          'ios 6',
          'android 4'
        ]
      },
      dev: {
        files: {
          'css/style.prefixed.css': [
            'css/style.unprefixed.css'
          ]
        }
      },
      dist: {
        files: {
          'css/style.prefixed.css': [
            'css/style.unprefixed.css'
          ]
        }
      }
    },

    /**
     * CSSMin
     * Minificación de CSS
     * https://github.com/gruntjs/grunt-contrib-cssmin
     * Minifica css/style.min.css y lo guarda en css/style.prefixed.css o style.unprefixed.css,
     * dependiendo del modo (desarrollo o producción)
     */
    cssmin: {
      dev: {
        files: {
          'css/style.min.css': [
            'css/style.prefixed.css'
          ]
        }
      },
      dist: {
        files: {
          'css/style.min.css': [
            'css/style.prefixed.css'
          ]
        }
      }
    },

    /**
     * Limpiar archivos
     * https://github.com/gruntjs/grunt-contrib-clean
     */
    clean: {
      dist: [
        'css/style.css',
        'css/style.unprefixed.css',
        'css/style.prefixed.css'
      ]
    },

    /**
     * Lanza tareas cuando cambian los archivos
     * https://github.com/gruntjs/grunt-contrib-watch
     */
    watch: {
      sass: {
        files: 'sass/{,*/}*.{scss,sass}',
        tasks: ['scsslint', 'sass:dev', 'autoprefixer:dev', 'cssmin:dev']
      }
    }
  });

  /**
   * Desarrollo
   */
  grunt.registerTask('default', [
    'scsslint',
    'sass:dev',
    'autoprefixer:dev',
    'cssmin:dev',
    'watch'
  ]);

  /**
   * Producción
   */
  grunt.registerTask('build', [
    'sass:dist',
    'autoprefixer:dist',
    'cssmin:dist',
    'clean:dist'
  ]);

};
